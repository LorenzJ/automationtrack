class View
{
    playButton: HTMLElement;
    stopButton: HTMLElement;
    clearButton: HTMLElement;
    addButton: HTMLElement;
    resultsList: HTMLElement;
    elapsedTimeOutput: HTMLElement;

    constructor()
    {
        this.playButton = document.getElementById("play_button");
        this.stopButton = document.getElementById("stop_button");
        this.clearButton = document.getElementById("clear_button");
        this.addButton = document.getElementById("add_button");
        this.resultsList = document.getElementById("results");
        this.elapsedTimeOutput = document.getElementById("elapsed");
    }

    set paused(value:boolean)
    {
        this.playButton.innerText = !value ? "||" : "►";
    }

    set elapsedTime(value:number)
    {
        let formattedTime = View.formatTime(value);
        this.elapsedTimeOutput.innerText = formattedTime;
        document.title = formattedTime;
    }

    private static formatTime(elapsedTime:number):string
    {
        let elapsedDate = new Date(elapsedTime);
        let elapsedMilliseconds = elapsedDate.getUTCMilliseconds();
        let elapsedSeconds = elapsedDate.getUTCSeconds();
        let elapsedMinutes = elapsedDate.getUTCMinutes();
        let elapsedHours = elapsedDate.getUTCHours();
        
        let formatter = new Intl.NumberFormat("en-US", { minimumIntegerDigits: 2 });
        let fracFormatter = new Intl.NumberFormat("en-US", { minimumIntegerDigits: 3 });
        let formattedTime = 
            formatter.format(elapsedHours) + ":" +
            formatter.format(elapsedMinutes) + ":" +
            formatter.format(elapsedSeconds) + "." +
            fracFormatter.format(elapsedMilliseconds);

        return formattedTime;
    }

    addResult(lastElapsedTime:number, elapsedTime:number):void
    {
        let item = document.createElement("li");
        let deltaTime = elapsedTime - lastElapsedTime;
        item.innerText = View.formatTime(elapsedTime) + " | " + View.formatTime(deltaTime);
        item.appendChild(document.createElement("input"));
        this.resultsList.appendChild(item);
    }

    removeResults():void
    {
        this.resultsList.innerHTML = "";
    }

}