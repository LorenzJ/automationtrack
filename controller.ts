class Controller
{
    private view: View;
    private stopwatch: Stopwatch;
    private lastElapsedTime: number;

    constructor(view:View)
    {
        this.view = view;
        this.stopwatch = new Stopwatch(window);
        this.lastElapsedTime = null;

        view.playButton.addEventListener("click", () => this.pauseOrResume());
        view.stopButton.addEventListener("click", () => this.stop());
        view.clearButton.addEventListener("click", () => this.removeResults());
        view.addButton.addEventListener("click", () => this.addResult());

        window.setTimeout(() => this.update(), 0);
    }

    private update():void
    {
        this.view.elapsedTime = this.stopwatch.elapsedTime;
        window.setTimeout(() => this.update(), 0);
    }

    pauseOrResume():void
    {
        if (!this.stopwatch.isPaused)
        {
            this.stopwatch.pause();
            this.view.paused = true;
        }
        else
        {
            this.stopwatch.resume();
            this.view.paused = false;
        }
    }

    stop():void
    {
        this.stopwatch.stop();
        this.view.paused = true;
    }

    removeResults():void
    {
        this.lastElapsedTime = null;
        this.view.removeResults();
    }

    addResult():void
    {
        let elapsedTime = this.stopwatch.elapsedTime;
        this.view.addResult(this.lastElapsedTime, elapsedTime);
        this.lastElapsedTime = elapsedTime;
    }
}