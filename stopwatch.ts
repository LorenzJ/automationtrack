class Stopwatch
{
    private startTime: number;
    private currentTime: number;
    private timeOffset: number;
    private timeoutHandle: number;
    private window: Window;
    private paused: boolean;

    constructor(window:Window)
    {
        this.startTime = 0;
        this.currentTime = 0;
        this.timeOffset = 0;
        this.timeoutHandle = null;
        this.window = window;
        this.paused = true;
    }
    
    resume()
    {
        this.paused = false;
        this.startTime = performance.now();
        this.timeoutHandle = this.window.setTimeout(() => this.update(), 10);
    }
    
    pause()
    {
        this.paused = true;
        this.timeOffset = this.elapsedTime;
        this.startTime = 0;
        this.currentTime = 0;
        this.window.clearTimeout(this.timeoutHandle);
    }

    private update()
    {
        this.currentTime = performance.now();
        this.timeoutHandle = this.window.setTimeout(() => this.update(), 10);
    }

    get isPaused()
    {
        return this.paused
    }

    get elapsedTime()
    {
        return this.currentTime - this.startTime + this.timeOffset;
    }
    
    stop()
    {
        this.pause();
        this.timeOffset = 0;
        this.currentTime = 0;
        this.startTime = 0;
    }
}